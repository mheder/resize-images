# Resize images



## Getting started

Use the following command to resize jpeg files in a directory:

```
for i in $(find * -type f -iname '*.jpg' ! -iname 're_*.jpg'); do convert -resize 40% $i re_$i; done
```

The command is case insensitive.
